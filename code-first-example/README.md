###################################################################
# Integrating Open API with spring boot for Code First Approach
###################################################################

### Steps

```steps
[REQUIRED STEP]
1. Add following dependency to pom
<dependency>
	<groupId>org.springdoc</groupId>
	<artifactId>springdoc-openapi-ui</artifactId>
	<version>1.2.32</version>
</dependency>

[REQUIRED STEP]
2. Create or Use existing Open API configuration spring class, add below annotation
@OpenAPIDefinition(
        info = @Info(
                title = "Code First Open API spec Info Title"
                ,version = "1.0"
                ,description = "Code First Open API spec Info Title Description"))
@Configuration
public class OpenApiConfig {
}
 
```

### Steps to start this application
```steps
1. clone this repo.
2. cd /open-api/code-first-example
3. open the maven project and run
4. application will be hosted on port 8081 
```

### Urls

```urls
Once the service is up and running we cann access
1. Swagger
{baseUrl}/swagger-ui.html

2. open api-docs
{baseUrl}/v3/api-docs

3. download open api-docs yaml file
{baseUrl}/v3/api-docs.yaml

where
baseUrl e.g - http://127.0.0.1:8081
```