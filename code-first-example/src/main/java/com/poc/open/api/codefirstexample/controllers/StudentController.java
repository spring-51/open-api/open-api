package com.poc.open.api.codefirstexample.controllers;

import com.poc.open.api.codefirstexample.dto.requests.StudentRequest;
import com.poc.open.api.codefirstexample.dto.responses.StudentResponse;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
public class StudentController implements StudentApi {

    @Override
    public StudentResponse save(StudentRequest request){
        StudentResponse studentResponse = new StudentResponse(request.getId(), request.getName());
        return studentResponse;
    }
    @Override
    public StudentResponse update(StudentRequest request){
        StudentResponse studentResponse = new StudentResponse(request.getId(), request.getName());
        return studentResponse;
    }

    @Override
    public StudentResponse patch(StudentRequest request){
        StudentResponse studentResponse = new StudentResponse(request.getId(), request.getName());
        return studentResponse;
    }

    @Override
    public StudentResponse getStudent(@PathVariable Long id){
        return new StudentResponse(id, "test");
    }

    @Override
    public List<StudentResponse> getAll(){
        StudentResponse studentResponse = new StudentResponse(1l, "test");
        List<StudentResponse> responses = new ArrayList<>();
        responses.add(studentResponse);
        return responses;
    }

}
