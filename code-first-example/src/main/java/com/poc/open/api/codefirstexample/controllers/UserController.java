package com.poc.open.api.codefirstexample.controllers;

import com.poc.open.api.codefirstexample.dto.requests.UserRequest;
import com.poc.open.api.codefirstexample.dto.responses.UserResponse;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
public class UserController implements UserApi {

    @Override
    public UserResponse save(UserRequest request){
        UserResponse UserResponse = new UserResponse(request.getId(), request.getName());
        return UserResponse;
    }
    @Override
    public UserResponse update(UserRequest request){
        UserResponse UserResponse = new UserResponse(request.getId(), request.getName());
        return UserResponse;
    }

    @Override
    public UserResponse patch(UserRequest request){
        UserResponse UserResponse = new UserResponse(request.getId(), request.getName());
        return UserResponse;
    }

    @Override
    public UserResponse getUser(@PathVariable Long id){
        return new UserResponse(id, "test");
    }

    @Override
    public List<UserResponse> getAll(){
        UserResponse UserResponse = new UserResponse(1l, "test");
        List<UserResponse> responses = new ArrayList<>();
        responses.add(UserResponse);
        return responses;
    }

}
