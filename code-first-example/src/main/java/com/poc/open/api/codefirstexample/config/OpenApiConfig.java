package com.poc.open.api.codefirstexample.config;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import org.springframework.context.annotation.Configuration;


@OpenAPIDefinition(
        info = @Info(
                title = "Code First Open API spec Info Title"
                ,version = "1.0"
                ,description = "Code First Open API spec Info Title Description"))
@Configuration
public class OpenApiConfig {
}
