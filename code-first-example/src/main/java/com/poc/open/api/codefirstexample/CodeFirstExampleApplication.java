package com.poc.open.api.codefirstexample;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CodeFirstExampleApplication {

    public static void main(String[] args) {
        SpringApplication.run(CodeFirstExampleApplication.class, args);
    }

}
