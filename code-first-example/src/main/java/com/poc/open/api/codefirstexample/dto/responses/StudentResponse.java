package com.poc.open.api.codefirstexample.dto.responses;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class StudentResponse {
    public StudentResponse() {
    }


    public StudentResponse(Long id,String name) {
        this.id = id;
        this.name = name;
    }

    private Long id;
    private String name;

}
