package com.poc.open.api.codefirstexample.controllers;

import com.poc.open.api.codefirstexample.dto.requests.StudentRequest;
import com.poc.open.api.codefirstexample.dto.responses.StudentResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.context.annotation.EnableMBeanExport;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RequestMapping("students")
// @Tag is used to logically Group our API
// its Optional
@Tag(
        name = "Students" // default value is student-controller
        ,description = "Student Resource specific API"
)
public interface StudentApi {

    @PostMapping
    @Operation(
            summary = "Create new Student Resource Summary",
            description = "Create new Student Resource Description"
    )
    StudentResponse save(@RequestBody StudentRequest request);

    @PutMapping
    @Operation(
            summary = "Update Student Resource Summary",
            description = "Update Student Resource Description"
    )
    StudentResponse update(@RequestBody StudentRequest request);

    @PatchMapping
    @Operation(
            summary = "Patch Student Resource Summary",
            description = "Patch Student Resource Description"
    )
    StudentResponse patch(@RequestBody StudentRequest request);

    @GetMapping(value = "{id}")
    @Operation(
            summary = "Get Student Resource By Id Summary",
            description = "Get Student Resource By Id Description"
    )
    StudentResponse getStudent(Long id);

    @GetMapping
    @Operation(
            summary = "Get all active Student Resource Summary",
            description = "Get all active Student Resource Description"
    )
    List<StudentResponse> getAll();
}
