package com.poc.open.api.codefirstexample.controllers;

import com.poc.open.api.codefirstexample.dto.requests.UserRequest;
import com.poc.open.api.codefirstexample.dto.responses.UserResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping("users")
// @Tag is used to logically Group our API
// its Optional
@Tag(
        name = "Users" // default value is User-controller
        ,description = "User Resource specific API"
)
public interface UserApi {

    @PostMapping
    @Operation(
            summary = "Create new User Resource Summary",
            description = "Create new User Resource Description"
    )
    UserResponse save(@RequestBody UserRequest request);

    @PutMapping
    @Operation(
            summary = "Update User Resource Summary",
            description = "Update User Resource Description"
    )
    UserResponse update(@RequestBody UserRequest request);

    @PatchMapping
    @Operation(
            summary = "Patch User Resource Summary",
            description = "Patch User Resource Description"
    )
    UserResponse patch(@RequestBody UserRequest request);

    @GetMapping(value = "{id}")
    @Operation(
            summary = "Get User Resource By Id Summary",
            description = "Get User Resource By Id Description"
    )
    UserResponse getUser(Long id);

    @GetMapping
    @Operation(
            summary = "Get all active User Resource Summary",
            description = "Get all active User Resource Description"
    )
    List<UserResponse> getAll();
}
